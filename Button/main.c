//Program testing pressing on-board button
#include "stm32f0xx.h"

//GOPI and LED defines
#define GPIO_MODER_OUT 1

#define LED_PORT GPIOC
#define BLUE_LED_BIT 8
#define GREEN_LED_BIT 9

#define BUTTON_PORT GPIOA
#define BUTTON_BIT 0

//SYSTICK defines
#define SYSCLK_FREQ	HSI_VALUE

#define SYSTICK_FREQ 100 // number in hz 

void SystemInit(void)
{
	//??
	FLASH->ACR = FLASH_ACR_PRFTBE;
}

int main(void)
{
	//AHB peripheral clock enable register
	//RCC_AHBENR_GPIOCEN - GPIOC clock enable
	//RCC_AHBENR_GPIOAEN - GPIOA clock enable
	RCC->AHBENR = RCC_AHBENR_GPIOCEN | RCC_AHBENR_GPIOAEN;
	//GPIO port mode register
	//MODERy[1:0] - 01: General purpose output mode
	//!!!
	//MODERy[1:0] - 00: Input mode (reset state) -> thats why BUTTON port is not configured
	//!!!!
	// GREEN_LED_BIT << 1 means GREEN_LED_BIT*2 
	LED_PORT->MODER = GPIO_MODER_OUT << (GREEN_LED_BIT << 1) | GPIO_MODER_OUT << ( BLUE_LED_BIT << 1 );
	//GPIO port output data register
	LED_PORT->ODR = 1 << GREEN_LED_BIT;
	//SysTick configuration for 100hz = 10ms
	SysTick_Config(SYSCLK_FREQ / SYSTICK_FREQ);                    
	//
	SCB->SCR = SCB_SCR_SLEEPONEXIT_Msk;
	//
	__WFI();
}

void SysTick_Handler(void)
{
	static uint8_t bstate = 0;
	if( (bstate = (bstate << 1 & 0xf) | (BUTTON_PORT->IDR >> BUTTON_BIT & 1)) == 1)
	{
		LED_PORT->ODR ^= (1 << GREEN_LED_BIT) | (1 << BLUE_LED_BIT);
	}
}
