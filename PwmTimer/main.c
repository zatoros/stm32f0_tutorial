//PWM using timer test
#include "stm32f0xx.h"

//GOPI and LED defines
#define GPIO_MODER_AF 2

#define LED_PORT GPIOC
#define BLUE_LED_BIT 8
#define GREEN_LED_BIT 9

#define BUTTON_PORT GPIOA
#define BUTTON_BIT 0

//SYSTICK defines
#define SYSCLK_FREQ	HSI_VALUE
#define SYSTICK_FREQ 100 // number in hz 

//PWM Definers

#define PWM_FREQ 		400 //Hz
#define PWM_STEPS	80	//Light steps
#define PWM_CLK		HSI_VALUE
#define BLUE_LED_PWM TIM3->CCR3
#define GREEN_LED_PWM TIM3->CCR4
#define LED_OFF 0
#define LED_MAX (PWM_STEPS-1)

void SystemInit(void)
{
}

int main(void)
{
	//AHB peripheral clock enable register
	//RCC_AHBENR_GPIOCEN - GPIOC clock enable
	//RCC_AHBENR_GPIOAEN - GPIOA clock enable
	RCC->AHBENR = RCC_AHBENR_GPIOCEN | RCC_AHBENR_GPIOAEN;
	//APB peripheral clock enable register 1
	//RCC_APB1ENR_TIM3EN - Timer 3 clock enable
	RCC->APB1ENR = RCC_APB1ENR_TIM3EN;
	//GPIO port mode register
	//MODERy[1:0] - 10: Alternate function mode
	//MODERy[1:0] - 00: Input mode (reset state) -> thats why BUTTON port is not configured
	// GREEN_LED_BIT << 1 means GREEN_LED_BIT*2 
	LED_PORT->MODER = GPIO_MODER_AF << (GREEN_LED_BIT << 1) | GPIO_MODER_AF << ( BLUE_LED_BIT << 1 );
	//Prescaler register
	TIM3->PSC = (PWM_CLK / PWM_FREQ / PWM_STEPS) - 1;
	//Auto Reload register
	TIM3->ARR = PWM_STEPS - 1;
	//capture/compare register
	BLUE_LED_PWM = LED_OFF;
	GREEN_LED_PWM = LED_MAX;
	//capture/compare mode register
	TIM3->CCMR2 = TIM_CCMR2_OC4M_2 | TIM_CCMR2_OC4M_1 | //PWM mode 1
								TIM_CCMR2_OC4PE | //  preload enable
								TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | //PWM mode 1
 								TIM_CCMR2_OC3PE; //  preload enable
	//capture/compareenable register
	TIM3->CCER = TIM_CCER_CC4E | // Capture/Compare 4 output enable
							 TIM_CCER_CC3E;	// Capture/Compare 3 output enable
	//DMA/Interrupt enable register 
	TIM3->DIER = TIM_DIER_UIE;//Update interrupt enable
	//control register 1
	TIM3->CR1 = TIM_CR1_ARPE | //Auto-reload preload enable 1 - register is buffered
							TIM_CR1_CEN; //Counter enable
	//
	NVIC->ISER[0] = 1 << TIM3_IRQn;
	//SysTick configuration for 100hz = 10ms
	SysTick_Config(SYSCLK_FREQ / SYSTICK_FREQ);                    
	//
	SCB->SCR = SCB_SCR_SLEEPONEXIT_Msk;
	//
	__WFI();
}

void SysTick_Handler(void)
{
	static uint8_t ledPwmTemp = LED_OFF;
	static uint8_t direction = 0;
	
	if(direction == 0)
		ledPwmTemp++;
	else
		ledPwmTemp--;
		
	if(ledPwmTemp == LED_MAX)
		direction = 1;
	if(ledPwmTemp == LED_OFF)
		direction = 0;
		
		BLUE_LED_PWM = ledPwmTemp;
		GREEN_LED_PWM = LED_MAX - ledPwmTemp;
}

void TIM3_IRQHandler(void)
{
	TIM3->SR = ~TIM_SR_UIF;
}
