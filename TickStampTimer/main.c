//Program useing systick to blink leds
#include "stm32f0xx.h"

//GOPI and LED defines
#define GPIO_MODER_OUT 1

#define LED_PORT GPIOC
#define BLUE_LED_BIT 8
#define GREEN_LED_BIT 9

//SYSTICK defines
#define SYSCLK_FREQ	HSI_VALUE

#define SYSTICK_FREQ 100 // number in hz 
#define BLINK_PERIOD 50 // number in systick's

void SystemInit(void)
{
}

int main(void)
{
	//AHB peripheral clock enable register
	//RCC_AHBENR_GPIOCEN - GPIOC clock enable
	RCC->AHBENR = RCC_AHBENR_GPIOCEN;
	//GPIO port mode register
	//MODERy[1:0] - 01: General purpose output mode
	// GREEN_LED_BIT << 1 means GREEN_LED_BIT*2 
	LED_PORT->MODER = GPIO_MODER_OUT << (GREEN_LED_BIT << 1) | GPIO_MODER_OUT << ( BLUE_LED_BIT << 1 );
	//GPIO port output data register
	LED_PORT->ODR = 1 << GREEN_LED_BIT;
	//SysTick configuration for 100hz = 10ms
	SysTick_Config(SYSCLK_FREQ / SYSTICK_FREQ);                    
	//
	SCB->SCR = SCB_SCR_SLEEPONEXIT_Msk;
	//
	__WFI();
}

void SysTick_Handler(void)
{
	//init blink period
	static uint8_t blink_timer = BLINK_PERIOD;
	//count
	if(--blink_timer == 0)
	{
		//reset blink period
		blink_timer = BLINK_PERIOD;
		//TOGGLE LEDs
		LED_PORT->ODR ^= ( 1 << GREEN_LED_BIT ) | ( 1 << BLUE_LED_BIT );
	}
}
