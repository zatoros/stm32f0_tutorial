﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

//Helping script creating new project with desired name from Empty
//Script dediceted to MY code organization and validation is poor :-)

namespace CreateNewProjectScript
{
    class Program
    {
        static readonly string sourceProjectPath = @".\Empty";

        static void Main(string[] args)
        {
            string projectName;

            Console.Write("Type project name :");

            projectName = Console.ReadLine();
            while (!validateProjectName(projectName))
            {
                Console.WriteLine("Project name is not valid, try again or type empty name to cancel!");
            }

            if (projectName == "")
                return;

            DirectoryInfo dir = new DirectoryInfo(sourceProjectPath);
            DirectoryInfo[] dirs = dir.GetDirectories();
            string destProjectPath = @".\" + projectName;

            Console.WriteLine("Checking directories...");
            if (!dir.Exists)
            {
                Console.WriteLine("Project \'Empty\' not found");
                return;
            }

            Console.WriteLine("Creating directories...");
            if (Directory.Exists(destProjectPath))
            {
                Console.WriteLine("Project with name \'" + projectName + "\' already exists");
                return; 
            }
            Directory.CreateDirectory(destProjectPath);

            Console.WriteLine("Copying files...");
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                if (file.Name == "main.c" || file.Name == "startup_stm32f0xx.s")
                {
                    string temppath = Path.Combine(destProjectPath, file.Name);
                    file.CopyTo(temppath, false);
                }
                else if (file.Name == "Empty.uvproj" || file.Name == "Empty.uvopt")
                {
                    string temppath = Path.Combine(destProjectPath, file.Name.Replace("Empty",projectName));
                    File.WriteAllText(temppath, file.OpenText().ReadToEnd().Replace("Empty", projectName));
                }
            }

            foreach (DirectoryInfo subdir in dirs)
            {
                string temppath = Path.Combine(destProjectPath, subdir.Name);
                Directory.CreateDirectory(temppath);
            }
        }

        static public bool validateProjectName(string projectName)
        {
            Regex containsABadCharacter = new Regex("[" + Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars())) + "]");
            if (containsABadCharacter.IsMatch(projectName)) { return false; };
            // other checks for UNC, drive-path format, etc
            return true;
        }
    }
}
