//UART (without DMA for now) test
//Sends echo of what recived
//commands
//255 - manual ( sending 0 - LED_MAX controls leds )
//254 - auto ( potenctiometer controls leds like in adc test)
#include "stm32f0xx.h"

//GOPI and LED defines
#define GPIO_MODER_AF 2

#define LED_PORT GPIOC
#define BLUE_LED_BIT 8
#define GREEN_LED_BIT 9

#define BUTTON_PORT GPIOA
#define BUTTON_BIT 0

//SYSTICK defines
#define SYSCLK_FREQ	HSI_VALUE
#define SYSTICK_FREQ 100 // number in hz 

//PWM Definers

#define PWM_FREQ 		400 //Hz
#define PWM_STEPS	80	//Light steps
#define PWM_CLK		HSI_VALUE
#define BLUE_LED_PWM TIM3->CCR3
#define GREEN_LED_PWM TIM3->CCR4
#define LED_OFF 0
#define LED_MAX (PWM_STEPS-1)

#define NUM_ROUNDS 10

#define BAUND_RATE 9600

enum WORK_TYPE { AUTO, MANUAL } workType;

void SystemInit(void)
{
}

int main(void)
{
	workType = AUTO;
	//AHB peripheral clock enable register
	//RCC_AHBENR_GPIOCEN - GPIOC clock enable
	//RCC_AHBENR_GPIOAEN - GPIOA clock enable
	RCC->AHBENR = RCC_AHBENR_GPIOCEN | RCC_AHBENR_GPIOAEN;
	//APB peripheral clock enable register 1
	//RCC_APB1ENR_TIM3EN - Timer 3 clock enable
	RCC->APB1ENR = RCC_APB1ENR_TIM3EN;
	//ADC1 & USART1 clokc enable
	RCC->APB2ENR = RCC_APB2ENR_ADC1EN | RCC_APB2ENR_USART1EN;
	//???
	GPIOA->AFR[1] = 1 << (2 << 2) | 1 << (1 << 2);
	//PIN 10 PULL-DOWN??
	GPIOA->PUPDR = 2 << ( 10 << 1 );
	//KEEP SWD PINS, CHANGE PINS FUNC TO UART
	GPIOA->MODER = GPIO_MODER_AF << (14<<1) | GPIO_MODER_AF << (13<<1) |
			GPIO_MODER_AF << (10<<1) | GPIO_MODER_AF << (9<<1);
	//GPIO port mode register
	//MODERy[1:0] - 10: Alternate function mode
	//MODERy[1:0] - 00: Input mode (reset state) -> thats why BUTTON port is not configured
	// GREEN_LED_BIT << 1 means GREEN_LED_BIT*2 
	LED_PORT->MODER =GPIO_MODER_AF << (GREEN_LED_BIT << 1) | GPIO_MODER_AF << ( BLUE_LED_BIT << 1 );
	//Prescaler register
	TIM3->PSC = (PWM_CLK / PWM_FREQ / PWM_STEPS) - 1;
	//Auto Reload register
	TIM3->ARR = PWM_STEPS - 1;
	//capture/compare register
	BLUE_LED_PWM = LED_OFF;
	GREEN_LED_PWM = LED_MAX;
	//USART config
	//
	USART1->BRR = ( SYSCLK_FREQ + BAUND_RATE / 2 ) / BAUND_RATE;
	//level inversion
	//USART1->CR2 = USART_CR2_TXINV | USART_CR2_RXINV;
	//usart enable, transimiter enable, reciver enable, rx interrupt enable
	USART1->CR1 = USART_CR1_RXNEIE | USART_CR1_TE | USART_CR1_RE | USART_CR1_UE;
	//capture/compare mode register
	TIM3->CCMR2 = TIM_CCMR2_OC4M_2 | TIM_CCMR2_OC4M_1 | //PWM mode 1
								TIM_CCMR2_OC4PE | //  preload enable
								TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | //PWM mode 1
 								TIM_CCMR2_OC3PE; //  preload enable
	//capture/compareenable register
	TIM3->CCER = TIM_CCER_CC4E | // Capture/Compare 4 output enable
							 TIM_CCER_CC3E;	// Capture/Compare 3 output enable
	//DMA/Interrupt enable register 
	TIM3->DIER = TIM_DIER_UIE;//Update interrupt enable
	//control register 1
	TIM3->CR1 = TIM_CR1_ARPE | //Auto-reload preload enable 1 - register is buffered
							TIM_CR1_CEN; //Counter enable
	//enable interrupts
	NVIC->ISER[0] = 1 << TIM3_IRQn | 1 << USART1_IRQn;
	//ADC channel selection register
	ADC1->CHSELR = ADC_CHSELR_CHSEL10;
	//ADC sampling timeregister
	//110: 71.5 ADC clock cycles ( applies to ALL channels
	ADC1->SMPR = ADC_SMPR1_SMPR_1 | ADC_SMPR1_SMPR_2;
	//ADC common configuration register
	ADC->CCR = ADC_CCR_VREFEN; // //VREFEN Enable
	//ADC configuration register 1
	ADC1->CFGR1 = ADC_CFGR1_WAIT | //Wait conversion mode
								ADC_CFGR1_CONT; //Single / continuous conversion mode
	//ADC control register
	ADC1->CR = ADC_CR_ADCAL; //This bit is set by software to start the calibration of the ADC
	//SysTick configuration for 100hz = 10ms
	SysTick_Config(SYSCLK_FREQ / SYSTICK_FREQ);  
	
	//
	SCB->SCR = SCB_SCR_SLEEPONEXIT_Msk;
	//
	__WFI();
}

void SysTick_Handler(void)
{
	static uint32_t ledPwmTemp = LED_OFF;
	
	//ADC interrupt and status register 
	//End of conversion flag
	if(ADC1->ISR & ADC_ISR_EOC)
	{
		//ADC data register
		ledPwmTemp = ADC1->DR & 0xFFF;
		
		ledPwmTemp = (ledPwmTemp * LED_MAX) / 0xFFF;
		
	}
	//ADC ready
	else if(ADC1->ISR & ADC_ISR_ADRDY)
	{
		// ADC start conversion command & ADC enable command
		ADC1->CR = ADC_CR_ADSTART | ADC_CR_ADEN;
	}
	// !ADC start conversion command && !ADC enable command
	else if((ADC1->CR & (ADC_CR_ADSTART | ADC_CR_ADEN )) == 0)
	{
		//ADC enable command
		ADC1->CR = ADC_CR_ADEN;
	}
	
	if( workType == AUTO)
	{	
		BLUE_LED_PWM = ledPwmTemp;
		GREEN_LED_PWM = LED_MAX - ledPwmTemp;
	}
}

void TIM3_IRQHandler(void)
{
	TIM3->SR = ~TIM_SR_UIF;
}

void USART1_IRQHandler(void)
{
	if(USART1->ISR & USART_ISR_RXNE)
	{
		uint8_t rec = USART1->RDR;
		if(rec == 255)
		{
			workType = MANUAL;
		}
		else if( rec == 254)
		{
			workType = AUTO;
		}
		else if( workType == MANUAL && rec <= LED_MAX)
		{
			BLUE_LED_PWM = rec;
			GREEN_LED_PWM = LED_MAX - rec;
		}
		USART1->TDR = rec;
	}
}
