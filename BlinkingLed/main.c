//Program blinking two on board leds
#include "stm32f0xx.h"

#define GPIO_MODER_OUT 1

#define LED_PORT GPIOC
#define BLUE_LED_BIT 8
#define GREEN_LED_BIT 9

void SystemInit(void)
{
}

int main(void)
{
	uint32_t i;
	//AHB peripheral clock enable register
	//RCC_AHBENR_GPIOCEN - GPIOC clock enable
	RCC->AHBENR = RCC_AHBENR_GPIOCEN;
	//GPIO port mode register
	//MODERy[1:0] - 01: General purpose output mode
	// GREEN_LED_BIT << 1 means GREEN_LED_BIT*2 
	LED_PORT->MODER = GPIO_MODER_OUT << (GREEN_LED_BIT << 1) | GPIO_MODER_OUT << ( BLUE_LED_BIT << 1 );
	//GPIO port output data register
	LED_PORT->ODR = 1 << GREEN_LED_BIT;
	
	for(;;)
	{
		for(i = 0; i < 1000000; i++);
		//TOGGLE LEDs
		LED_PORT->ODR ^= ( 1 << GREEN_LED_BIT ) | ( 1 << BLUE_LED_BIT );
	}
}
